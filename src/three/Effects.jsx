import { useLoader } from "@react-three/fiber";
import { EffectComposer, SSR, Bloom, LUT } from "@react-three/postprocessing";
import { useControls } from "leva";
import { LUTCubeLoader } from "postprocessing";

export const Effects = () => {
  const texture = useLoader(LUTCubeLoader, "materials/F-6800-STD.cube");
  const { enabled, ...props } = useControls({
    enabled: true,
  
  });
  return (
    enabled && (
      <EffectComposer disableNormalPass>
        <Bloom
          luminanceThreshold={0.5}
          mipmapBlur
          luminanceSmoothing={1}
          intensity={0.5}
        />
        <LUT lut={texture} />
      </EffectComposer>
    )
  );
};
