import { Canvas } from '@react-three/fiber';
import './App.css';
import { Model_0 } from './models/Model_0';
import { ContactShadows, Environment, Lightformer, OrbitControls, PerspectiveCamera, Stage } from '@react-three/drei';
import { Model_1 } from './models/Model_1';
import { Model_2 } from './models/Model_2';
import { Model_3 } from './models/Model_3';
import { Model_4 } from './models/Model_4';
import { Model_5 } from './models/Model_5';
import { Model_6 } from './models/Model_6';
import { Model_7 } from './models/Model_7';
import { Model_8 } from './models/Model_8';
import { Model_17 } from './models/Model_17';
import { Model_19 } from './models/Model_19';
import { Model_20 } from './models/Model_20';
import { Model_9 } from './models/Model_9';
import { Model_10 } from './models/Model_10';
import { Effects } from './three/Effects';

function App() {
  return (
    <div className="App">
      <Canvas camera={{position:[70,40,70]}}>
        <color attach="background" args={['#15151a']} />
        <hemisphereLight intensity={0.5} />

        <ContactShadows resolution={1024} frames={1} position={[0, -1.16, 0]} scale={15} blur={0.5} opacity={1} far={20} />

        <group scale={[25, 1, 25]}>

          <mesh scale={4} position={[3, -1.161, -1.5]} rotation={[-Math.PI / 2, 0, Math.PI / 2.5]}>
            <ringGeometry args={[0.9, 1, 4, 1]} />
            <meshStandardMaterial color="white" roughness={0.75} />
          </mesh>
          <mesh scale={4} position={[-3, -1.161, -1]} rotation={[-Math.PI / 2, 0, Math.PI / 2.5]}>
            <ringGeometry args={[0.9, 1, 3, 1]} />
            <meshStandardMaterial color="white" roughness={0.75} />
          </mesh>
        </group>

        <group position={[0, -1.5, 0]}>
          <Model_0 />
          <Model_1 />
          <Model_2 />
          <Model_3 />
          <Model_4 />
          <Model_5 />
          <Model_6 />
          <Model_7 />
          <Model_8 />
          <Model_9 />
          <Model_10 />
          {/* <Model_17 /> */}
          <Model_19 />
          <Model_20 />
        </group>

        <Environment resolution={512}>
          {/* Ceiling */}
          <Lightformer intensity={2} rotation-x={Math.PI / 2} position={[0, 4, -9]} scale={[10, 1, 1]} />
          <Lightformer intensity={2} rotation-x={Math.PI / 2} position={[0, 4, -6]} scale={[10, 1, 1]} />
          <Lightformer intensity={2} rotation-x={Math.PI / 2} position={[0, 4, -3]} scale={[10, 1, 1]} />
          <Lightformer intensity={2} rotation-x={Math.PI / 2} position={[0, 4, 0]} scale={[10, 1, 1]} />
          <Lightformer intensity={2} rotation-x={Math.PI / 2} position={[0, 4, 3]} scale={[10, 1, 1]} />
          <Lightformer intensity={2} rotation-x={Math.PI / 2} position={[0, 4, 6]} scale={[10, 1, 1]} />
          <Lightformer intensity={2} rotation-x={Math.PI / 2} position={[0, 4, 9]} scale={[10, 1, 1]} />
          {/* Sides */}
          <Lightformer intensity={2} rotation-y={Math.PI / 2} position={[-50, 2, 0]} scale={[100, 2, 1]} />
          <Lightformer intensity={2} rotation-y={-Math.PI / 2} position={[50, 2, 0]} scale={[100, 2, 1]} />
          {/* Key */}
          <Lightformer form="ring" color="#01377D" intensity={10} scale={2} position={[10, 5, 10]} onUpdate={(self) => self.lookAt(0, 0, 0)} />
        </Environment>

        <Effects />
        <OrbitControls makeDefault  args={[10,10,10]}  />
      </Canvas>
    </div>
  );
}

export default App;
